public class Subiect1 {
    class I implements Y {
        private long t;

        public void f() {
            K k = new K();
        }
    }

    class J {
        public void i(I i) {
        }
    }

    class K {
        private M m;
        private L l;
    }

    class L {
        public void metA() {
        }
    }

    class M {
        public void metB() {
        }
    }

    class N {
        I i = new I();
    }

    interface Y {
        public void f();
    }
}

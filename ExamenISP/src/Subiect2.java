import javax.swing.*;
import java.awt.*;

public class Subiect2 extends JFrame {
    JTextArea textArea = new JTextArea();
    JButton button = new JButton();
    JTextField textField = new JTextField();

    public Subiect2(){
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(300, 60);
        this.setLayout(new GridLayout(1, 3));
        button.setText("Execute");
        button.addActionListener(e -> {
            textArea.setText(textField.getText());
        });
        this.add(textField);
        this.add(button);
        this.add(textArea);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        Subiect2 subiect2 = new Subiect2();
    }
}
